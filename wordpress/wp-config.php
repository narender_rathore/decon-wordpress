<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3}o&qz$:shR.(Z:U(A8Qs{99Jk[t+o&t FVes4-Y1mw_R<U)mONn[(MaE7:%DV1E');
define('SECURE_AUTH_KEY',  '?Q &(]nclT0|=Ii3a%tTLYfXwO,>:AXB)CCrGe&);/)uWD/jIDm?KR,?Z <muACm');
define('LOGGED_IN_KEY',    'B}f0Iq|Bl$2YR{~*MFU}J~C|{/4ZaQ|F`Yl.Oa(D*+C7_~8So[#M_&HM/q~$b96b');
define('NONCE_KEY',        '|lYs.tzfDlS C|jW=YJ<ww3][7MhVzz&0k3JUrX%fBx17 zW}n+b^%+Q`~o>Xn83');
define('AUTH_SALT',        'ILaP lMy7r4[V-yV/_&X)zPZe1gq!KLuEjGLv4JSR^cPGX6?gas GU,-dwU7%Ibk');
define('SECURE_AUTH_SALT', ' v)-3`P+VYLI*kmG~p,^/72fw+++3ATH[edr~/d)ar/C&%l~HqiFP/+$,u#pik*P');
define('LOGGED_IN_SALT',   'Bi94ojGVq6rwe6v&~$B3Q4j)DUP*uQ<X+xUdX4@8Pm`KcNB$d+]}H$s(,#0Tp)A4');
define('NONCE_SALT',       'tjOvtCpn@tGD8SO}Fch*)tg+6@/}&h9]rX/[#]4D[G_oVysgGB[z >81f<<N&D|#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
