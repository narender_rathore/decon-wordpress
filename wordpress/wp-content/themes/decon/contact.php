<?php include_once('header.php'); ?>
<div class="container">
   <div class="col-sm-4 col-sm pull-left"  style=" border-right: 1px solid #F8F8FF;">
        <div class="bs-example">
        <h2>CONTACT INFO</h2><hr>
        <ul class="list-unstyled">
        <li><p><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<b>Manes Winchester</b><br> Family Law Firm<br> the address city, state 1111</p>
        </li> 
		<li><p><span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;PHONE: (+20) 000222999</p>
        </li>
        <li><p><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;Email: info@email.com</p>
        </li>
         </ul>
        </div>
    </div>
 <div class="col-sm-8 col-sm pull-right">
 <h1>CONTACTS</h1>
 <div class="section">
 <h2>SEND US A QUICK MESSAGE</h2>
 <p>if yo are havingproblemediting this website template, then don't hesitate to ask for help on the Forums</p>
 <form class="form-horizontal" role="form" method="post" action="index.php">
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label">Name</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
		</div>
	</div>
	<div class="form-group">
		<label for="message" class="col-sm-2 control-label">Message</label>
		<div class="col-sm-10">
			<textarea class="form-control" rows="4" name="message"></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<! Will be used to display an alert to the user>
		</div>
	</div>
</form>


 </div>
 <div class="clearfix"></div>
</div>

</div>

<?php include_once('footer.php'); ?>